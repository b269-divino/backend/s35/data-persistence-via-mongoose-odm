/*open gitbash
npm init  -y
npm install express mongoose
npm install -g nodemon
touch .gitignore index.js
*/


const express = require("express");

// Mongoose is a package that allows creating of Schemas to our model to our data structures
// Also has acccess to a number of methods for manipulating our database
const mongoose = require("mongoose")

const app = express();
const port = 3001;

// Connecting to MongoDB Atlas

// mongoose.connect ("mongodb+srv://joeydivino:<(change the password to default password)>admin123@zuitt-bootcamp.tkfjv2z.mongodb.net/-->>(enter database name)<--?retryWrites=true&w=majority",


mongoose.connect ("mongodb+srv://joeydivino:admin123@zuitt-bootcamp.tkfjv2z.mongodb.net/s35?retryWrites=true&w=majority",
	{
		// allows us to avoid any current and future errors while connecting to MongoDB
		useNewUrlParser: true,
		useUnifiedTopology: true
	}

);

// Allows to handle errors when initial connection is established
let db = mongoose.connection;


//console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));


// Connecting to MongoDb Atlas End

app.use(express.json());
app.use(express.urlencoded({extended:true}));


// [SECTION] Mongoose Schemas

const taskSchema = new mongoose.Schema ({
	username: String,
	password: String,
	status: {
		type: String,
		default: "Pending"
	}
});

// [SECTION] Mongoose Models
//models must be in singular form and capitalized
const Task = mongoose.model("Task", taskSchema);


// creation of task application
// create a new task
/*
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/signup", (req, res) => {
	Task.findOne({ username: req.body.username }).then((result, err ) => {
		
		if(result != null && result.username == req.body.username) {
			return res.send("Duplicate user found");
		} else {
			let newTask = new Task ({
				username: req.body.username
			});
			newTask.save().then((savedTask, saveErr) =>
			{
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New user registered!");
				}
			})
		}
	})

});



//Getting all the tasks
/*

BUSINESS LOGIC
Retrieve all the documents
If an error is encountered, print the error
If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/signup", (req, res) => {
	Task.find({}).then((result, err) => {
		if (err) {
			return console.log(err);
		} else {
			return res.status(200).json ({
				data: result
			})
		}
	})
})




app.listen(port, () => console.log(`Server is running at ${port}`));





















